package com.felipe.felipe.navigationdiefrent;

import android.content.ClipData;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ExpandedMenuView;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuPopupHelper;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
ExpandedMenuView expandedMenuView;
    List<String> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = new ArrayList<>();
        list.add("Hola");
        list.add("Hola");
        list.add("Hola");
        list.add("Hola");
        list.add("Hola");
        expandedMenuView = (ExpandedMenuView) findViewById(R.id.expanded_menu);


        MenuBuilder menuBuilder = new MenuBuilder(this);
        menuBuilder.add("Hola");
        expandedMenuView.initialize(menuBuilder);
    }
  /*  private void init() {
        MenuBuilder mMenuBuilder;
        mMenuBuilder = new MenuBuilder(getContext());
      MenuPopupHelper mMenuPopupHelper = new MenuPopupHelper(MainActivity.this, mMenuBuilder, this);

    }*/

}
