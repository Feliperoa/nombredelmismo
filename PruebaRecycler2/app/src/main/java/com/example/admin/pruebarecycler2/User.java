package com.example.admin.pruebarecycler2;

/**
 * Created by ADMIN on 18/07/2017.
 */

public class User {
    public static final int ITEMLEFT =0;
    public static final int ITEMRIGTH =1;


    private String name;
    private String photo;
    private int tipo;

    public User() {
    }

    public User(String name, String photo, int tipo) {
        this.name = name;
        this.photo = photo;
        this.tipo = tipo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", photo='" + photo + '\'' +
                ", tipo='" + tipo + '\'' +
                '}';
    }
}
