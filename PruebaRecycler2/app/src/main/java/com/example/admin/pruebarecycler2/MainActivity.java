package com.example.admin.pruebarecycler2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
RecyclerView recyclerView;
    List<User> users;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        users = new ArrayList<>();
        llenar();
        listar();

    }

    private void listar() {
        if (users.size() > 0) {
            RecyclerAdapter recyclerAdapter = new RecyclerAdapter(users, MainActivity.this, new RecyclerAdapter.OnItemClick() {
                @Override
                public void OnClick(User user, int position, int type) {
                    Log.e("LOGG", "esto es lo que clickea: " + user.toString());
                }
            });
            recyclerView.setAdapter(recyclerAdapter);

        }
    }

    public void llenar() {
        users.add(new User("primero","null",0));
        users.add(new User("segundo","null",1));
        users.add(new User("primero","null",1));
        users.add(new User("primero","null",1));
        users.add(new User("segundo","null",0));
        users.add(new User("primero","null",1));
        users.add(new User("primero","null",1));
        users.add(new User("primero","null",1));
        users.add(new User("primero","null",1));
        users.add(new User("segundo","null",0));
        users.add(new User("segundo","null",0));
        users.add(new User("segundo","null",0));
        users.add(new User("segundo","null",0));
        users.add(new User("segundo","null",0));
        users.add(new User("segundo","null",0));

    }
}
