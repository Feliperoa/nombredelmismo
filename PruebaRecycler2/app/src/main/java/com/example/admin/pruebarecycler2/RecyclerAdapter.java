package com.example.admin.pruebarecycler2;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ADMIN on 18/07/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<User> list;
    public Context context;
    int type_views;
    public interface OnItemClick{
        void OnClick(User user, int position, int type);
    }
    OnItemClick listener;

    public RecyclerAdapter(List<User> list, Context context, OnItemClick listener) {
        this.list = list;
        type_views = list.size();
        this.context = context;
        this.listener = listener;
    }
    public class RecyclerItemLeft extends RecyclerView.ViewHolder{
        ImageView img;
        ImageButton btn1,btn2,btn3;
        TextView txt;
        public RecyclerItemLeft(View v) {
            super(v);
            img = (ImageView) v.findViewById(R.id.img);
            btn1 = (ImageButton) v.findViewById(R.id.imageButton);
            btn2 = (ImageButton) v.findViewById(R.id.imageButton2);
            btn3 = (ImageButton) v.findViewById(R.id.imageButton3);
        }

        public void bind(final User user, final int position, final OnItemClick onItemClick) {
            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.OnClick(user,position,1);
                }
            });
            btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.OnClick(user,position,2);
                }
            });
            btn3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.OnClick(user,position,2);
                }
            });
        }

    }
    public class RecyclerItemRigth extends RecyclerView.ViewHolder{
        ImageView img;
        ImageButton btn1,btn2,btn3;
        TextView txt;
        public RecyclerItemRigth(View v) {
            super(v);
            img = (ImageView) v.findViewById(R.id.img);
            btn1 = (ImageButton) v.findViewById(R.id.imageButton);
            btn2 = (ImageButton) v.findViewById(R.id.imageButton2);
            btn3 = (ImageButton) v.findViewById(R.id.imageButton3);
        }

        public void bind(final User user, final int position, final OnItemClick onItemClick) {
            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.OnClick(user,position,1);
                }
            });
            btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.OnClick(user,position,2);
                }
            });
            btn3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.OnClick(user,position,2);
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case User.ITEMLEFT:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item1, parent, false);
                return new RecyclerItemLeft(v);
            case User.ITEMRIGTH:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item2, parent, false);
                return new RecyclerItemRigth(v);

        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        switch (list.get(position).getTipo()) {
            case 0:
                return  User.ITEMLEFT;
            case 1:
                return User.ITEMRIGTH;
            default:return -1;

        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        User user = list.get(position);
        if (user != null) {
            switch (user.getTipo()) {
                case User.ITEMLEFT:
                    ((RecyclerItemLeft)holder).bind(list.get(position),position,listener);
                    break;
                case User.ITEMRIGTH:
                    ((RecyclerItemRigth)holder).bind(list.get(position),position,listener);
                    break;
            }
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
